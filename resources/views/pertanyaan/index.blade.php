@extends('adminlte.master')

@section('content')
<div class="p-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif
            <a class="btn btn-primary mb-3" href="/pertanyaan/create">Buat Pertanyaan</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Judul</th>
                        <th>Isi</th>
                        <th style="width: 40px">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($pertanyaan as $key => $q)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$q -> judul}}</td>
                        <td>{{$q -> isi}}</td>
                        <td style="display: flex;">
                            <a href="/pertanyaan/{{$q->id}}" class="btn btn-info btn-sm mr-1">detail</a>
                            <a href="/pertanyaan/{{$q->id}}/edit" class="btn btn-default btn-sm mr-1">edit</a>
                            <form action="/pertanyaan/{{$q->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger btn-sm" value="hapus">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">Tidak ada data</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@endsection